# Mobile Phone Charging Station

![](/pictures/result.jpeg)

## Description

Here you can find the complete project description for the replica of my cell phone charging station. The charging station can charge up to 5 phones in fast charge mode.  The charging current is measured and colorful animations are played, depending on how much power the phone is drawing.

Since the charging electronics regulate down as soon as the phone is fully charged, you can see by the color and speed whether the phone is still charging or already fully charged.  Depending on the charging power, the color will change from green (low power) to blue to deep red. 

The FastCharge modules installed inside have normal USB ports and support the most common manufacturers. Some phones can be supplied with up to 24W charging power. Therefore, it is important to get an appropriately sized power supply. 

## Parts

### Electronic Parts
- 5x Fast Charge Modules, QC 3,0 QC 2,0 USB DC-DC Buck Converter ([Ali-Link](https://www.aliexpress.com/item/1005003715656205.html?spm=a2g0o.order_list.0.0.21ef5c5fYgH6FB)) The fast charging modules are available with different number of channels. Please make sure to buy the 1-channel version, because we want to measure the current individually for each cell phone.
- 5x INA219 I2C interface Bi-directional current/power monitoring sensor module ([Ali-Link](https://de.aliexpress.com/item/32381557070.html?gatewayAdapt=glo2deu&spm=a2g0o.order_list.0.0.21ef5c5fYgH6FB))
- 5x WS2812B LED Pixel Individually Addressable Ring 8 ([Ali-Link](https://www.aliexpress.com/item/1005002304069758.html?spm=a2g0o.order_list.0.0.21ef5c5fYgH6FB))
- 1x Mini DC-DC 12-24V To 5V 3A Step Down Power Supply Module Buck Converter ([Ali-Link](https://www.aliexpress.com/item/1005001621804332.html?spm=a2g0o.productlist.0.0.51b7112d1aFoR1&algo_pvid=4fb9825b-f073-4843-92ca-86e59eb98ecd&aem_p4p_detail=2022040711425112973301173088100001858518&algo_exp_id=4fb9825b-f073-4843-92ca-86e59eb98ecd-39&pdp_ext_f=%7B%22sku_id%22%3A%2212000016846372615%22%7D&pdp_pi=-1%3B0.86%3B-1%3B1.16%40salePrice%3BEUR%3Bsearch-mainSearch))
- 12V power supply, at least 120W  ([Amazon](https://www.amazon.de/gp/product/B08XWJFGFB/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1))
- 1x ESP8266 WeMos D1 mini
- A strip grid board
- A hollow plug, stranded wire, solder the usual odds and ends


### Housing

- 8mm Plywood
- 3mm Plexiglass [BLACK & WHITE 9H04 SC](https://www.plexiglas-shop.com/en-de/products/plexiglas-led/pl9h04-sc-3-00-3050x2030-b-01-s.html?listtype=search&searchparam=PLEXIGLAS®%20LED%2C%20Black%20%26%20White%209H04%20SC))
- Black PLA Filament, I prefer Extrudr NX2 matte black ([Amazon](https://www.amazon.de/-/en/extrudr®-PLA-3D-Printer-Filament/dp/B0741CGGDG/ref=sr_1_1?crid=3E6D7HWEK8ASD&keywords=extrudr+nx2+schwarz+matt&qid=1646768123&sprefix=extrudr+nx2+schwarz+matt%2Caps%2C66&sr=8-1))
- Magnets ([Amazon](https://www.amazon.de/gp/product/B07T13H3R5/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1))
- Superglue, Woodglue

# Assembly

For the assembly you need a CNC mill and a 3D printer. A laser cutter is advantageous for cutting out the Plexiglas, but not absolutely necessary.

## Wooden parts

I milled the plywood with my MPCNC and a 1/8" single blade cutter. Except for the routing for the hollow plug and the recess for the mobiles, the wood is simply cut. Please make sure to mill out the corners with curves, otherwise the pegs will not fit in the recesses. I glued the individual housing parts together with normal wood glue.

## The LED circles

For the LED rings, the housings must be printed out. The STL files are located in the folder "prints". The lid consists of a simple circle that has to be milled or lasered out of the plexiglass mentioned above. I deliberately didn't upload a vector file, because you might need two or three tries to find the correct radius including the deviations. The disc should simply click into the housing and then hold by itself.

![](/pictures/ledcase.jpeg)


## The PCB

Please excuse the quick and dirty schematic. But I think you can see how everything is wired. The Step Down Converter supplies the ESP and the INA sensors with constant 5V. The sensors are connected via the standard I2C interface.

![](/pictures/pcb.jpeg)

One more thing needs to be noted here: The INA breakout boards have only two contacts to set the I2C addresses. This results in 4 possible combinations. Oh, wait... we have 5 sensors.  But this is not a big drama. A look at the data sheet reveals that even more combinations are possible. But for this you have to improvise a bit. 
I decided to connect A0 with SDA. First you have to unsolder the small pulldown resistor.  If everything works, the sensor should get the I2C address 0x42... 

![](/pictures/fifth-ina219.jpeg)

To flash the software you need Visual Studio with Platform IO. All dependencies are defined by platformio.ini. The project should compile on the first try. If you have assigned the I2C addresses as above, no changes to the code should be necessary. Inital the Neopixel LED rings are controlled via GPIO D4. 

# License

This project is licensed under the Creative Commons Attribution Non Commercial Share Alike 4.0 International



